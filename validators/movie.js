const {check} = require("express-validator");
const validateResults = require("../utils/handleValidator")

const validatorCreateItem = [
    check("Title").exists().notEmpty(),
    check("Year").exists().notEmpty(),
    check("Released").exists().notEmpty(),
    check("Genre").exists().notEmpty(),
    check("Director").exists().notEmpty(),
    check("Actors").exists().notEmpty(),
    check("Plot").exists().notEmpty(),
    check("Ratings").exists().notEmpty(),
    check("Ratings.Source").exists().notEmpty(),
    check("Ratings.Value").exists().notEmpty(),
    (req, res, next) => {
        return validateResults(req, res, next);
    }
]

const validatorGetItem = [
    check("title").exists().notEmpty(),
    (req, res, next) => {
        return validateResults(req, res, next);
    }
]

module.exports = {validatorGetItem, validatorCreateItem};