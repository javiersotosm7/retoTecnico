const mongoose = require("mongoose");
const MovieSchema = new mongoose.Schema(
    {
        Title:{
            type: String,
            unique: true
        },
        Year:{
            type: String
        },
        Released:{
            type: String
        },
        Genre:{
           type: String
        },
        Director:{
            type: String
        },
        Actors:{
            type: String
        },
        Plot:{
            type: String
        },
        Ratings:[
            {
                Source: {
                    type: String
                },
                Value: {
                    type: String
                },
            }
        ]
    },
    {
        timestamps: true, // TODO createdAt, updatedAt
        versionKey: false 
    }
);

module.exports = mongoose.model("movie", MovieSchema);