const express = require("express");
const router =  express.Router();
const { validatorGetItem, validatorCreateItem} = require("../validators/movie");
const {getMovie, getMovies, updateMovie } = require("../controllers/movie");


/** 
 * Obtener todas las películas
 */
router.get("/",getMovies); 
 /** 
  * Buscador de películas
  */
router.get("/:title", validatorGetItem,getMovie); 

 /**
  * Buscar y reemplazar
  */
router.post("/:title",validatorGetItem, updateMovie); 

module.exports = router;