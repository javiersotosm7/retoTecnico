const {movieModel} = require('../models');
const {matchedData} = require("express-validator")
const {handleHttpError} = require('../utils/handleError')
const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));


/**
 * Obtener una lista de datos
 * @param {*} req 
 * @param {*} res 
 */
const getMovies = async (req, res) => {
    const { page = 1, limit = 5 } = req.query;
    try {
        const data = await movieModel.find({}).limit(limit*1).skip((page -1) * limit).exec()
        res.send({ data })

    }catch(e){
        handleHttpError(res,"ERROR_EN_GET_ITEMS")
        console.log(e)
    }

};

/**
 * Obtener un detalle
 * @param {*} req 
 * @param {*} res 
 */
const getMovie = async (req, res) => {
    const apiUrl = process.env.API_URL
    try {
        const { title } = req.params;
        const { year } = req.headers;
        const regex = new RegExp(title, 'i') 
        const find = await movieModel.findOne({ Title: { $regex: regex }})


        if(find === null)
        {   
            if(year === undefined)
            {
                const body = await fetch(`${apiUrl}, ${title}`).then(response => response.json());
                const data = await movieModel.create(body);
                res.send(data)
            }
            else
            {
                const body = await fetch(`${apiUrl}, ${title},'&y=',${year}`).then(response => response.json());
                const data = await movieModel.create(body);
                res.send(data)
            }
            
        }
        else
        {
            res.send(find)
        }
        
        
    }catch(e){
        handleHttpError(res,"ERROR_EN_GET_MOVIE");
        console.log(e);
    }
};

/**
 * actualizar
 * @param {*} req 
 * @param {*} res 
 */
const updateMovie = async (req, res) => {
    try {
        const { movie, find, replace } = req.body;
        const regex = new RegExp(movie, 'i') 
        const data = await movieModel.findOne({ Title: { $regex: regex }})
        const nuevo = data.Plot.toString().replace(find, replace)
        
        res.send(nuevo)

    }catch(e){
        handleHttpError(res,"ERROR_EN_UPDATE_ITEMS")
        console.log(e);
    }
};

module.exports = {getMovie, getMovies, updateMovie};